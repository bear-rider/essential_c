#include <stdio.h>

int main(void)
{
    int a = 0;

    while (a < 5)
    {
        printf("a = %d\n", a);
        a++;
    }
    printf("and finally a = %d\n", a);
}