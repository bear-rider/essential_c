#include <stdio.h>

int sum_and_dff(int a, int b, int *res)
{
    int sum;
    sum = a + b;
    *res = a - b;
    return sum;
}

int main(void){
    int b = 2;
    int diff;

    //sum but pass a pointer for diff
    printf ("The sum of 5 and %d is %d\n", b, sum_and_diff (5, b, &diff));

    // use that pointer now
    printf ("The difference of 5 and %d is %d\n", b, diff);
}