#include <stdio.h>

int main(void)
{
    int a = 8;

    switch (a)
    {
    case 0:
        printf("a = %d\n", a);
        break;
    case 1:
        printf("a = %d\n", a);
        break;
    default:
        printf("a = %d\n", a);
    }
}