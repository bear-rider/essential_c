#include <stdio.h>
int main(void)
{
    int a = 0;
    while (1)
    {
        printf("a = %d\n", a);
        a++;
        if (a == 5)
        {
            break;
        }
    }
    printf("and finally a = %d\n", a);
}